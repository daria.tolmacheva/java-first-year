//class of the addition progtam
public class AddQuadPoly
{
  public static void main (String[] args)
  {
    //create instenses of polynomials' class
    //for first addition term
    QuadPoly firstPoly = new QuadPoly(Double.parseDouble(args[1]),
                                      Double.parseDouble(args[2]),
                                      Double.parseDouble(args[3]));
    //for second addition
    QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[4]),
                                       Double.parseDouble(args[5]),
                                       Double.parseDouble(args[6]));
    //for the sum
    QuadPoly sumPoly = new QuadPoly(firstPoly.secondCoof + secondPoly.secondCoof,
                                    firstPoly.firstCoof + secondPoly.firstCoof,
                                    firstPoly.zeroCoof + secondPoly.zeroCoof);

    //print the result
    System.out.println("Polynomial: " + firstPoly.secondCoof + "x^2 + "
                         + firstPoly.firstCoof + "x +" + firstPoly.zeroCoof);
    System.out.println("added to: " + secondPoly.secondCoof + "x^2 + "
                         + secondPoly.firstCoof + "x +" + secondPoly.zeroCoof);
    System.out.println("results in: " + sumPoly.secondCoof + "x^2 + "
                         + sumPoly.firstCoof + "x +" + sumPoly.zeroCoof);
  }//end main
}//end AddQuadPoly
