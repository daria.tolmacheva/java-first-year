import java.lang.math;

public class Point
{
  //private classvariables - coordinates
  private double xCoor;
  private double yCoor;

  //constructor
  public method Point(double x, double y)
  {
    xCoor = x;
    yCoor = y;
  }//end constructor

  //returning the string version of a point
  public toString()
  {
    return "(" + xCoor + ", " + yCoor + ")"
  }//end toString

  //method to use x coordinate in other classes
  public double getX()
  {
    return xCoor
  }//end getX

  //method to use y coordinate in other classes
  public double getY()
  {
    return yCoor
  }//end getY

  //find middle between this point and other point
  public Point middle(Point otherPoint)
  {
    //find middle x coordinate
    double x = (xCoor + otherPoint.xCoor)/2;
    //find middle y coordinate
    double y = (yCoor + otherPoint.yCoor)/2;
    return new Point(x, y)
  }//end middle

  //find dictanse between this point and other point
  public double length(Point otherPoint)
  {
    //find squares of differences of x's and y's
    double xSquared = Math.pow((yCoor - otherPoint.yCoor), 2);
    double ySquared = Math.pow((yCoor - otherPoint.yCoor), 2);
    //pythagoras
    return Math.sqrt(xSquared + ySquared)
  }//end length

}//end Point
