//class of the polynomials
public class QuadPoly
{
  //instanse variables
  public double secondCoof;
  public double firstCoof;
  public double zeroCoof;

  //constructor method
  public QuadPoly(double secondC, double firstC, double zeroC)
  {
    secondCoof = secondC;
    firstCoof = firstC;
    zeroCoof = zeroC;
  }//end constructor
} //end QuadPoly
