public class MarkAnalysis
{
  public static void main(String[] args)
  {
    Scanner scanner = Scanner(System.in);
    System.out.print("Enter the number of marks: ");
    int numOfMarks = scanner.nextInt();
    System.out.print("\n");
    if (numOfMarks < 1)
      System.out.println("You need at least 1 score.");
    else
    {
      //array to store marks
      int[] marksArray = new int[numOfMarks];

      //loop to get marks
      for (index = 1; index <= numOfMarks; index++)
      {
        System.out.print("Enter mark # " + index + ": ");
        marksArray[index] = scanner.nextInt();
        System.out.print("\n");
      }//end first for

      //initialize min, max and sum fot further use
      int min = marksArray[0];
      int max = 0;
      int sum = 0;

      //loop to get the stats
      for (index = 1; index <= numOfMarks; index++)
      {
         sum += marksArray[index];
         if (marksArray[index] > max)
           max = marksArray[index];
         if (marksArray[index] < min)
           min = marksArray[index];
      }//end second for

      //calculate mean
      double mean = sum/parseDouble(numOfMarks);

      //output stats
      System.out.println(" The mean mark is: " + mean);
      System.out.println(" The maximum mark is: " + max);
      System.out.println(" The minimum mark is: " + min);

      //output marks
      System.out.println("Person | Score | difference from mean");
      for (index = 1; index <= numOfMarks; index++)
      {
        System.out.printf("%6d | %5d | %6.2f%n", (index + 1),
                          marksArray[index],
                          (marksArray[index] - mean));
      }//end last for

    }//end else
  }//end main
}//end class
