public class Student
{
  private final String name;
  private final int mark;

  //constructor
  public void Student(String studentName, int score)
  {
    name = studentName;
    mark = score;
  }//end constructor

  //give access to name from other classes
  public String getName()
  {
    return name;
  }//end get name

  //get access to mark from other classes
  public String getMark()
  {
    return mark;
  }//end get mark

  //compare students by mark then by name
  public int compareTo(Student other)
  {
    if (mark == other.getMark)
      return name.compareTo(other.getName());
    else
      return (mark - other.getMark);
  }//end compare


  //convert to srting
  public String toString()
  {
    return ("%-10s got %5d", name, mark);
  }//end to string

}//end student
