public class MarkAnalysisSortedObj
{
  public static void main(String[] args)
  {
    Scanner scanner = Scanner(System.in);
    System.out.print("Enter the number of marks: ");
    int numOfMarks = scanner.nextInt();
    System.out.print("\n");
    if (numOfMarks < 1)
      System.out.println("You need at least 1 score.");
    else
    {
      //array to store marks
      Student[] marksArray = new Student[numOfMarks];
      int name;
      int mark;
      //loop to get marks
      for (index = 1; index <= numOfMarks; index++)
      {
        System.out.print("Enter the name of student "
                         + index + ": ");
        name = scanner.nextLine();
        System.out.print("\n");
        System.out.print("Enter mark for '" + name + "': ");
        mark = scanner.nextInt();
        System.out.print("\n");
        marksArray[index - 1] = new Student(name, mark);
      }//end first for

      //sort the array
      bubbleSort(marksArray[], numOfMarks);

      //initialize sum for further use
      int sum = 0;

      //loop to get the sum
      for (index = 1; index <= numOfMarks; index++)
      {
         sum += marksArray[index].getMark();
      }//end second for

      //calculate mean
      double mean = sum/parseDouble(numOfMarks);
      int max = marksArray[numOfMarks].getMark();
      int min = marksArray[0].getMark();

      //output stats
      System.out.println(" The mean mark is: " + mean);
      System.out.println(" The maximum mark is: " + max);
      System.out.println(" The minimum mark is: " + min);

      //output marks
      System.out.println("Person | Score | difference from mean");
      for (index = 1; index <= numOfMarks; index++)
      {
        System.out.printf("%6d | %5d | %6.2f%n", (index + 1),
                          marksArray[index],
                          (marksArray[index] - mean));
      }//end last for

    }//end else
  }//end main

  //method that sorts an array in assending order
  public static void bubbleSort(int[] anArray, int lengthOfArray)
  {
    //flag to stop the outer loop
    boolean swapped;
    //max index for inner loop
    int unsortedLength = lengthOfArray;
    do
    {
      swapped = false;
      for (int index = 1; index < unsortedLength - 1; index++)
      {
        if (anArray[index].compareTo(anArray[index - 1]) < 0)
        {
          int currentValue = anArray[index];
          anArray[index] = anArray[index - 1];
          anArray[index - 1] = currentValue;
        }//end if
      }//end for
      unsortedLength--
    } while (swapped);
  }//end sort

}//end class
