import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.swing.JButton;
import java.swing.JFrame;
import java.swing.JLabel;

public class StopClock extends JFrame implements ActionListener
{
  private boolean isRunning = false;
  private long startTime = 0;
  private long stopTime = 0;

  private final startTimeJLabel = new JLabel("Not Started");
  private final stopTimeJLabel = new JLabel("Not Started");
  private final elapsedTimeJLabel = new JLabel("Not Started");

  //Container
  public StopClock()
  {
    setTitle("Stop Clock");
    Container contents = getContainerPane();
    contents.setLayout(new GridLayout(0,1));

    contents.add(new JLabel("Started at: "));
    contents.add(startTimeJLabel);

    contents.add(new JLabel("Stopped at: "));
    contents.add(stopTimeJLabel);

    contents.add(new JLabel("Elapsed time (seconds): "));
    contents.add(elapsedTimeJLabel);

    JButton startStopJButton = new JButton(" Start | Stop ");

    startStopJButton.addActionListner(this);
    contents.add(startStopJButton);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//end constructor

  //actions after pressing
  public static void main(String[] args)
  {
    if (!isRunning)
    {
      startTime = System.currentTimeMillis();
      startTimeJLabel.setText("" + startTime);
      stopTimeJLabel.setText("Running...");
      elapsedTimeJLabel.setText("Running...");
      isRunning = true;
    }
    else
    {
      stop



    }

  }

}
