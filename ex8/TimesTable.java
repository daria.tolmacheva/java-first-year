import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class TimesTable extends JFrame
{
  //constor
  public method TimesTable(int constMult, int maxFirstMult)
  {
    setTitle("MultTable");
    Container contents = getContentPane();
    contents.setLayout(new GridLayout(maxFirstMult, 5, 40, 25));
    for (index = 1; index <= maxFirstMult; index++)
    {
      contents.add(new JLabel("" + index));
      contents.add(new JLabel("x"));
      contents.add(new JLabel("" + constMult));
      contents.add(new JLabel("="));
      contents.add(new JLabel("" + (index * constMult)));
    }
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//end method

  //now output
  public static void main(String[] args)
  {
    TimesTable theTimesTable = new TimesTable(args[0], args[1]);
    theHelloFamily.setVisible(true);
  }//end main
}//end TimesTable
