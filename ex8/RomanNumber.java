public class RomanNumber
{
  //declare different forms of a number
  private int intValue;
  private String strValue;

  //first constructor - integer
  public method RomanNumber(int intEquivalent)
  {
    intValue = intEquivalent;
    romanValue = "";
  }//end integer constructor

  public method intToRoman()
  {
    //untill all of the number is done
    while (intValue > 0)
    {
      //check the value and add greatest to the roman
      if (intValue >= 1000)
      {
        intValue -= 1000;
        romanValue = romanValue + "M";
      }
      else if (intValue >= 900)
      {
        intValue -= 900;
        romanValue = romanValue + "CM";
      }
      else if (intValue >= 500)
      {
        intValue -= 500;
        romanValue = romanValue + "D";
      }
      else if (intValue >= 400)
      {
        intValue -= 400;
        romanValue = romanValue + "CD";
      }
      else if (intValue >= 100)
      {
        intValue -= 100;
        romanValue = romanValue + "C";
      }
      else if (intValue >= 90)
      {
        intValue -= 90;
        romanValue = romanValue + "XC";
      }
      else if (intValue >= 50)
      {
        intValue -= 50;
        romanValue = romanValue + "L";
      }
      else if (intValue >= 40)
      {
        intValue -= 40;
        romanValue = romanValue + "XL";
      }
      else if (intValue >= 10)
      {
        intValue -= 10;
        romanValue = romanValue + "X";
      }
      else if (intValue >= 9)
      {
        intValue -= 9;
        romanValue = romanValue + "IX";
      }
      else if (intValue >= 5)
      {
        intValue -= 5;
        romanValue = romanValue + "V";
      }
      else if (intValue >= 4)
      {
        intValue -= 4;
        romanValue = romanValue + "IV";
      }
      else
      {
        intValue -= 1;
        romanValue = romanValue + "I";
      }//endif
    }//end while
  }//end intToRoman

  //second constructor - string
  public method RomanNumber(int strForm)
  {
    romanValue = strForm;
    intValue = 0;
  }//end string constructor

  public method romanToInt()
  {
    for (index = 0; index < romanValue.length(); index++)
    {
      //decryption code goes here
    }//end for
  }//end romanToInt

}//end class
