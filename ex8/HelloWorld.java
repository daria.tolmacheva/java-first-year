import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloWorld extends JFrame
{
  //construct it
  public HelloWorld()
  {
    setTitle("HelloWorld");
    Container contents = getContentPane();
    contents.add(new JLabel("Привет всем, коротающим "
                            + "свой век на планете Земля."));
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//end method HelloWorld

  //now actually output it
  public static void main(String[] args)
  {
    HelloWorld theHelloWorld = new HelloWorld();
    theHelloWorld.setVisible(true);
  }//end main
}//end class HelloWorld
