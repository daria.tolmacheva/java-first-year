import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloFamily extends JFrame
{
  //construct it
  public HelloWorld()
  {
    setTitle("Hello Family");
    Container contents = getContentPane();
    //single line thus
    contents.setLayout(new FlowLayout());
    contents.add(new JLabel("Hello, Liza!"));
    contents.add(new JLabel("Hello, Tania!"));
    contents.add(new JLabel("Hello, Sergey!"));
    contents.add(new JLabel("Hello, Galya!"));
    contents.add(new JLabel("Hello, Olya!"));
    contents.add(new JLabel("Hello, Nina!"));
    contents.add(new JLabel("Hello, Egor!"));
    contents.add(new JLabel("Hello, Veronika!"));
    contents.add(new JLabel("Hello, Lera!"));

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//end method HelloWorld

  //now actually output it
  public static void main(String[] args)
  {
    HelloFamily theHelloFamily = new HelloFamily();
    theHelloFamily.setVisible(true);
  }//end main
}//end class HelloFamily
