import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloFamily extends JFrame
{
  //construct it
  public HelloWorld(int nRows, int nColumns)
  {
    setTitle("Hello Family");
    Container contents = getContentPane();
    //nRows x nColumns (one of) thus
    contents.setLayout(new GridLayout(nRows, nColumns, 30, 15));
    contents.add(new JLabel("Hello, Liza!"));
    contents.add(new JLabel("Hello, Tania!"));
    contents.add(new JLabel("Hello, Sergey!"));
    contents.add(new JLabel("Hello, Galya!"));
    contents.add(new JLabel("Hello, Olya!"));
    contents.add(new JLabel("Hello, Nina!"));
    contents.add(new JLabel("Hello, Egor!"));
    contents.add(new JLabel("Hello, Veronika!"));
    contents.add(new JLabel("Hello, Lera!"));

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//end method HelloWorld

  //now actually output it
  public static void main(String[] args)
  {
    HelloFamily theHelloFamily = new HelloFamily(args[0], args[1]);
    theHelloFamily.setVisible(true);
  }//end main
}//end class HelloFamily
