public class CompareQuadPoly
{
  public static void main(String[] args)
  {
    //create instenses of polynomials' class
    //for first addition term
    QuadPoly firstPoly = new QuadPoly(Double.parseDouble(args[1]),
                                      Double.parseDouble(args[2]),
                                      Double.parseDouble(args[3]));
    //for second addition
    QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[4]),
                                       Double.parseDouble(args[5]),
                                       Double.parseDouble(args[6]));

   boolean equal = firstPoly.equals(secondPoly);
   boolean smaller = firstPoly.lessThan(secondPoly);

   String comparison;

   if (equal)
     comparison = "is the same as: ";
   else if (smaller)
     comparison = "is the smaller than: ";
   else
     comparison = "is the greater than: ";

   System.out.println("Polynomial: " + firstPoly.secondCoof + "x^2 + "
                        + firstPoly.firstCoof + "x +" + firstPoly.zeroCoof);

   System.out.print(comparison);
   System.out.println(secondPoly.secondCoof + "x^2 + "
                        + secondPoly.firstCoof + "x +" + secondPoly.zeroCoof);

  }
}
