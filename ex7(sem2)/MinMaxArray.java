public class MinMaxArray
{
  public static <ArrayType extends Comparable<ArrayType>>
                Pair<ArrayType, ArrayType> getMinMax(ArrayType[] arrayToCompare)
				        throws IllegalArgumentException
  {
    if (anArray == null || arrayToCompare.length == 0)
      throw new IllegalArgumentException("Array does not exist or isn't empty.");

	  ArrayType max = arrayToCompare[0];
	  ArrayType min = arrayToCompare[0];
	  for(int index = 0; index < arrayToCompare.length; index++)
	  {
	    if (arrayToCompare[index].compareTo(max) > 0)
	      max = arrayToCompare[index];
  	  if (arrayToCompare[index].compareTo(min) < 0)
	      min = arrayToCompare[index];
	  }//end for

	  return new Pair<ArrayType, ArrayType>(max, min);
  }//getMinMax

}//MinMaxArray
