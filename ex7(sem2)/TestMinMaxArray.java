public class TestMinMaxArray
{
  public static void main(String[] args) throws RuntimeException
  {
    Pair<String, String> minMaxPair= MinMaxArray.<String>getMinMax(args);
  	String max = minMaxPair.getFirst();
	  String min = minMaxPair.getSecond();

	  System.out.println("Maximum of the given words is " + "'" + max + "'");
	  System.out.println("Minimum  of the given words is " + "'" + min + "'");

  } // main
} // class TestMinMaxArray
