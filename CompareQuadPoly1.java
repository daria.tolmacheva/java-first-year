//class of the polynomials
public class QuadPoly
{
  //instanse variables
  public double secondCoof;
  public double firstCoof;
  public double zeroCoof;

  //constructor method
  public QuadPoly(double secondC, double firstC, double zeroC)
  {
    secondCoof = secondC;
    firstCoof = firstC;
    zeroCoof = zeroC;
  }//end constructor

  //comparing whether they are equal method
  public boolean equals(double otherSecond, double otherFirst, double otherZero)
  {
    return secondCoof == otherSecond && firstCoof == otherFirst
                                     && zeroCoof == otherZero
  }//end comparing if equals

  //comparing whether one is less method
  public boolean lessThan(double otherSecond, double otherFirst, double otherZero)
  {
    return secondCoof < otherSecond
           || secondCoof == otherSecond
              && (firstCoof < otherFirst
                  || firstCoof == otherFirst && zeroCoof < otherZero)
  }//end comparing if less

} //end QuadPoly
